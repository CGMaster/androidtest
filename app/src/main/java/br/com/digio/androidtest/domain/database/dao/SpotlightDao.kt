package br.com.digio.androidtest.domain.database.dao

import androidx.room.Dao
import androidx.room.Query
import br.com.digio.androidtest.model.Spotlight

@Dao
interface SpotlightDao: BaseDao<Spotlight> {
    @Query("DELETE FROM spotlight")
    fun clear()

    @Query("SELECT * FROM spotlight")
    fun retrieveData(): List<Spotlight>
}