package br.com.digio.androidtest.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "cash")
data class Cash(
    val bannerURL: String,
    @PrimaryKey
    val title: String
)