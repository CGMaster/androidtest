package br.com.digio.androidtest.ui.adapter.product

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.androidtest.databinding.ItemMainProductsBinding
import br.com.digio.androidtest.model.Product
import br.com.digio.androidtest.util.load

class ProductItemViewHolder(
    binding: ItemMainProductsBinding
) : RecyclerView.ViewHolder(binding.root) {
    private val imageView = binding.imgMainItem
    private val progress = binding.progressImage

    fun bind(product: Product) {

        imageView.contentDescription = product.name
        progress.visibility = View.VISIBLE

        imageView.load(product.imageURL, progress)
    }
}
