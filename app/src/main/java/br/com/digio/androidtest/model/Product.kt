package br.com.digio.androidtest.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "products")
data class Product(
    val imageURL: String,
    @PrimaryKey
    val name: String,
    val description: String
)