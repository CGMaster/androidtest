package br.com.digio.androidtest.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "spotlight")
data class Spotlight(
    val bannerURL: String,
    @PrimaryKey
    val name: String,
    val description: String
)