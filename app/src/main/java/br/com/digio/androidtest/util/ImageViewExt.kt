package br.com.digio.androidtest.util

import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import br.com.digio.androidtest.R
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import java.lang.Exception

fun ImageView.load(url: String, progress:ProgressBar?=null){
    Picasso.get()
        .load(url)
        .error(R.drawable.ic_alert_circle)
        .into(this, object : Callback {
            override fun onSuccess() {
                progress?.visibility = View.GONE
            }

            override fun onError(e: Exception?) {
                progress?.visibility = View.GONE
            }
        })
}