package br.com.digio.androidtest.domain.database.dao

import androidx.room.Dao
import androidx.room.Query
import br.com.digio.androidtest.model.Product

@Dao
interface ProductDao: BaseDao<Product> {

    @Query("DELETE FROM products")
    fun clear()

    @Query("SELECT * FROM products")
    fun retrieveData(): List<Product>
}