package br.com.digio.androidtest.domain.database.di

import android.app.Application
import androidx.room.Room
import br.com.digio.androidtest.domain.database.CacheDatabase
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val databaseModule = module {
    single { provideDatabase(androidApplication()) }
}

fun provideDatabase(application: Application) =
    Room.databaseBuilder(application, CacheDatabase::class.java, "cache.db")
        .fallbackToDestructiveMigration()
        .build()