package br.com.digio.androidtest.domain.repository

import br.com.digio.androidtest.domain.database.CacheDatabase
import br.com.digio.androidtest.model.DigioProducts
import br.com.digio.androidtest.domain.service.DigioEndpoint
import retrofit2.HttpException

interface DigioRepository {
    suspend fun getProducts(): DigioProducts?
}

class DigioRepositoryImpl(
    private val cache: CacheDatabase,
    private val service: DigioEndpoint
): DigioRepository {

    override suspend fun getProducts(): DigioProducts? {
        return try {
            val response = service.getProducts()

            if(response.isSuccessful) {
                response.body()?.let {
                    saveProducts(it)
                    it
                }
            } else {
                throw HttpException(response)
            }
        } catch (e: Exception){
            e.printStackTrace()
            getCachedProducts()
        }
    }

    private fun saveProducts(products: DigioProducts) {
        cache.productDao().clear()
        cache.spotlightDao().clear()
        cache.cashDao().clear()

        cache.productDao().saveAll(products.products)
        cache.spotlightDao().saveAll(products.spotlight)
        cache.cashDao().save(products.cash)
    }

    private fun getCachedProducts(): DigioProducts? {
        return cache.cashDao().retrieveData()?.let{
            DigioProducts(
                cash = it,
                spotlight = cache.spotlightDao().retrieveData(),
                products = cache.productDao().retrieveData(),
            )
        }
    }
}