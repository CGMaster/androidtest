package br.com.digio.androidtest.domain.database.dao

import androidx.room.Dao
import androidx.room.Insert

@Dao
interface BaseDao<T> {
    @Insert
    fun saveAll(items: List<T>)

    @Insert
    fun save(item: T)
}