package br.com.digio.androidtest

import br.com.digio.androidtest.domain.database.di.databaseModule
import br.com.digio.androidtest.domain.di.domainModule
import br.com.digio.androidtest.domain.di.networkModule
import br.com.digio.androidtest.domain.di.repositoryModule
import br.com.digio.androidtest.ui.di.viewModelModule

val appComponent = listOf(
    networkModule,
    databaseModule,
    repositoryModule,
    domainModule,
    viewModelModule,
)