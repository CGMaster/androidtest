package br.com.digio.androidtest.domain.di

import br.com.digio.androidtest.domain.usecase.GetProducts
import org.koin.dsl.module

val domainModule = module {
    factory { GetProducts(get()) }
}