package br.com.digio.androidtest.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch

abstract class BaseViewModel: ViewModel() {
    val isLoading: MutableLiveData<Boolean> = MutableLiveData()
    val haveAnError: MutableLiveData<Boolean> = MutableLiveData()

    fun handleError(error: Exception){
        error.printStackTrace()
        haveAnError.postValue(true)
    }

    fun launch(action: suspend () -> Unit) {
        viewModelScope.launch(IO){
            try{
                action()
            } catch(e: Exception){
                e.printStackTrace()
            }
        }
    }
}