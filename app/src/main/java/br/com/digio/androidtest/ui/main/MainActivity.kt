package br.com.digio.androidtest.ui.main

import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.digio.androidtest.ui.adapter.product.ProductAdapter
import br.com.digio.androidtest.R
import br.com.digio.androidtest.ui.adapter.spotlight.SpotlightAdapter
import br.com.digio.androidtest.base.BaseActivity
import br.com.digio.androidtest.databinding.ActivityMainBinding
import br.com.digio.androidtest.model.Cash
import br.com.digio.androidtest.util.load
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>() {

    override val binding by lazy {
        ActivityMainBinding.inflate(layoutInflater)
    }
    override val viewModel by viewModel<MainViewModel>()

    private val productAdapter: ProductAdapter by lazy {
        ProductAdapter()
    }

    private val spotlightAdapter: SpotlightAdapter by lazy {
        SpotlightAdapter()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        with(binding){
            recyMainProducts.layoutManager = LinearLayoutManager(this@MainActivity, LinearLayoutManager.HORIZONTAL, false)
            recyMainProducts.adapter = productAdapter

            recyMainSpotlight.layoutManager = LinearLayoutManager(this@MainActivity, LinearLayoutManager.HORIZONTAL, false)
            recyMainSpotlight.adapter = spotlightAdapter

            body.visibility = View.GONE

        }
    }

    override fun observe(){
        viewModel.productsLiveData.observe(this){
            binding.body.visibility = View.VISIBLE

            productAdapter.products = it.products
            spotlightAdapter.spotlights = it.spotlight
            updateDigioCashSection(it.cash)
        }
        viewModel.isLoading.observe(this){
            binding.loadDigioContainer.root.visibility = if (it) { View.VISIBLE } else { View.GONE }
        }
        viewModel.haveAnError.observe(this){
            if (it){
                throwError()
                viewModel.haveAnError.value = false
            }
        }
    }

    private fun throwError(){
        val message = getString(R.string.error)
        binding.body.visibility = View.GONE

        Toast.makeText(this@MainActivity, message, Toast.LENGTH_SHORT).show()
    }

    private fun updateDigioCashSection(cash: Cash){
        setupDigioCashText(cash.title)
        binding.imgMainDigioCash.load(cash.bannerURL)
    }

    private fun setupDigioCashText(text: String) {
        binding.txtMainDigioCash.text = SpannableString(text).apply {
            setSpan(
                ForegroundColorSpan(
                    ContextCompat.getColor(this@MainActivity, R.color.blue_darker)
                ),
                0,
                5,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            setSpan(
                ForegroundColorSpan(
                    ContextCompat.getColor(this@MainActivity, R.color.font_color_digio_cash)
                ),
                6,
                text.length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }
    }

}