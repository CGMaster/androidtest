package br.com.digio.androidtest.domain.usecase

import br.com.digio.androidtest.domain.repository.DigioRepository
import br.com.digio.androidtest.model.DigioProducts

class GetProducts(private val repository: DigioRepository) {
    suspend operator fun invoke(): DigioProducts? =
        repository.getProducts()
}