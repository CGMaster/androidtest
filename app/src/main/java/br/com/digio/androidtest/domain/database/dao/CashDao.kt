package br.com.digio.androidtest.domain.database.dao

import androidx.room.Dao
import androidx.room.Query
import br.com.digio.androidtest.model.Cash

@Dao
interface CashDao: BaseDao<Cash> {
    @Query("DELETE FROM cash")
    fun clear()

    @Query("SELECT * FROM cash LIMIT 1")
    fun retrieveData(): Cash?
}