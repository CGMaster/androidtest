package br.com.digio.androidtest.domain.di

import br.com.digio.androidtest.BuildConfig
import br.com.digio.androidtest.domain.service.DigioEndpoint
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = module {
    factory{ provideDigioApi(get())}
    single { provideRetrofit() }
}

fun provideRetrofit(): Retrofit{
    val gson: Gson = GsonBuilder().create()
    val interceptor = HttpLoggingInterceptor().apply{
        level = HttpLoggingInterceptor.Level.BODY
    }
    val okHttp: OkHttpClient = OkHttpClient.Builder()
        .addInterceptor(interceptor)
        .build()

    return Retrofit.Builder()
        .baseUrl(BuildConfig.BASE_URL)
        .client(okHttp)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()
}

fun provideDigioApi(retrofit: Retrofit): DigioEndpoint = retrofit.create(DigioEndpoint::class.java)
