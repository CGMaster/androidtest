package br.com.digio.androidtest.ui.main

import androidx.lifecycle.MutableLiveData
import br.com.digio.androidtest.base.BaseViewModel
import br.com.digio.androidtest.domain.usecase.GetProducts
import br.com.digio.androidtest.model.DigioProducts

class MainViewModel(
    private val getProducts: GetProducts,
): BaseViewModel() {

    val productsLiveData: MutableLiveData<DigioProducts> = MutableLiveData()

    init {
        launch {
            isLoading.postValue(true)
            try{
                getProducts()?.let{
                    productsLiveData.postValue(it)
                } ?: throw Exception()
            } catch (e: Exception) {
                handleError(e)
            } finally {
                isLoading.postValue(false)
            }

        }
    }
}