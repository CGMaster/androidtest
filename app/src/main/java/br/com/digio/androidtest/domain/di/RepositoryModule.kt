package br.com.digio.androidtest.domain.di

import br.com.digio.androidtest.domain.repository.DigioRepository
import br.com.digio.androidtest.domain.repository.DigioRepositoryImpl
import org.koin.dsl.module

val repositoryModule = module {
    single<DigioRepository> { DigioRepositoryImpl(get(), get()) }
}