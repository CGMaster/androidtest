package br.com.digio.androidtest.ui.adapter.spotlight

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.androidtest.databinding.ItemMainSpotlightBinding
import br.com.digio.androidtest.model.Spotlight
import br.com.digio.androidtest.util.load

class SpotlightItemViewHolder(
    binding: ItemMainSpotlightBinding
) : RecyclerView.ViewHolder(binding.root) {
    private val imageView = binding.imgMainItem
    private val progress = binding.progressImage

    fun bind(product: Spotlight) {

        imageView.contentDescription = product.name
        progress.visibility = View.VISIBLE

        imageView.load(product.bannerURL, progress)
    }
}
