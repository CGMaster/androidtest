package br.com.digio.androidtest.domain.database

import androidx.room.Database
import androidx.room.RoomDatabase
import br.com.digio.androidtest.domain.database.dao.CashDao
import br.com.digio.androidtest.domain.database.dao.ProductDao
import br.com.digio.androidtest.domain.database.dao.SpotlightDao
import br.com.digio.androidtest.model.Cash
import br.com.digio.androidtest.model.Product
import br.com.digio.androidtest.model.Spotlight

@Database(version = 1, entities = [
    Product::class,
    Spotlight::class,
    Cash::class,
])
abstract class CacheDatabase: RoomDatabase() {
    abstract fun productDao(): ProductDao
    abstract fun spotlightDao(): SpotlightDao
    abstract fun cashDao(): CashDao
}