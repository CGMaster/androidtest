package br.com.digio.androidtest

import br.com.digio.androidtest.model.Cash
import br.com.digio.androidtest.model.DigioProducts
import br.com.digio.androidtest.model.Product
import br.com.digio.androidtest.model.Spotlight

val resultMock = DigioProducts(
    spotlight = listOf(
        Spotlight(
            name= "Recarga",
            bannerURL= "https://s3-sa-east-1.amazonaws.com/digio-exame/recharge_banner.png",
            description= "Agora ficou mais fácil colocar créditos no seu celular! A digio Store traz a facilidade de fazer recargas... direto pelo seu aplicativo, com toda segurança e praticidade que você procura."
        )
    ),
    products = listOf(
        Product(
            name= "XBOX",
            imageURL= "https://s3-sa-east-1.amazonaws.com/digio-exame/xbox_icon.png",
            description="Com o e-Gift Card Xbox você adquire créditos para comprar games, música, filmes, programas de TV e muito mais!"
        )
    ),
    cash = Cash(
        title= "digio Cash",
        bannerURL= "https://s3-sa-east-1.amazonaws.com/digio-exame/cash_banner.png",
    )
)