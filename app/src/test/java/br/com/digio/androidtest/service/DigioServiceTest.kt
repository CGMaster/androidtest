package br.com.digio.androidtest.service

import br.com.digio.androidtest.domain.service.DigioEndpoint
import br.com.digio.androidtest.getRetrofit
import br.com.digio.androidtest.resultMock
import br.com.digio.androidtest.successDispatcher
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit

class DigioServiceTest {

    private val server = MockWebServer()

    private val retrofit: Retrofit by lazy {
        server.getRetrofit()
    }

    private val service: DigioEndpoint by lazy {
        retrofit.create(DigioEndpoint::class.java)
    }

    @Before
    fun setup(){
        server.start()
        server.dispatcher = successDispatcher
    }

    @Test
    fun `endpoint sandbox-products return DigioProducts on success`() = runBlocking {
        // given
        val endpoint = "/sandbox/products"
        val expectedProducts = resultMock

        // when
        val actualProducts = service.getProducts().body()
        val request = server.takeRequest()

        // then
        assertEquals(request.path, endpoint)
        assertEquals(actualProducts, expectedProducts)
    }

    @After
    fun tearDown(){
        server.shutdown()
    }
}