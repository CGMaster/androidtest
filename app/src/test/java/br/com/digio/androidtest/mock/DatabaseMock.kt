package br.com.digio.androidtest.mock

import br.com.digio.androidtest.domain.database.CacheDatabase
import br.com.digio.androidtest.domain.database.dao.CashDao
import br.com.digio.androidtest.domain.database.dao.ProductDao
import br.com.digio.androidtest.domain.database.dao.SpotlightDao
import com.nhaarman.mockitokotlin2.mock

class DatabaseMock {
    val productDao = mock<ProductDao>()
    val spotlightDao = mock<SpotlightDao>()
    val cashDao = mock<CashDao>()

    companion object{
        //fun getMock() = CacheDatabase()
    }
}