package br.com.digio.androidtest.repository

import br.com.digio.androidtest.domain.database.CacheDatabase
import br.com.digio.androidtest.domain.database.dao.CashDao
import br.com.digio.androidtest.domain.database.dao.ProductDao
import br.com.digio.androidtest.domain.database.dao.SpotlightDao
import br.com.digio.androidtest.domain.repository.DigioRepositoryImpl
import br.com.digio.androidtest.domain.service.DigioEndpoint
import br.com.digio.androidtest.model.Cash
import br.com.digio.androidtest.model.DigioProducts
import br.com.digio.androidtest.model.Product
import br.com.digio.androidtest.model.Spotlight
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import retrofit2.HttpException
import retrofit2.Response

class DigioRepositoryTest {

    private val service = mock<DigioEndpoint>()
    private val database = mock<CacheDatabase>()
    private val repository = DigioRepositoryImpl(database, service)

    @Before
    fun setup(){
        val productDao = mock<ProductDao>()
        val spotlightDao = mock<SpotlightDao>()
        val cashDao = mock<CashDao>()

        whenever(database.productDao()).then{productDao}
        whenever(database.spotlightDao()).then{spotlightDao}
        whenever(database.cashDao()).then{cashDao}
    }

    @Test
    fun `getProducts() return products if server response is a success`() = runBlocking {
        // given
        val expectedProducts = mock<DigioProducts>()
        val response = Response.success(expectedProducts)

        whenever(service.getProducts()).thenReturn(response)

        //when
        val actualProducts = repository.getProducts()

        //then
        assertEquals(actualProducts, expectedProducts)
    }

    @Test
    fun `getProducts() return products if server response is a failure but CacheDatabase have data`() = runBlocking {
        // given
        val products = mock<List<Product>>()
        val spotlight = mock<List<Spotlight>>()
        val cash = mock<Cash>()
        val expectedProducts = DigioProducts(
            products = products,
            spotlight = spotlight,
            cash = cash,
        )
        val exception = mock<HttpException>()

        whenever(service.getProducts()).thenThrow(exception)
        whenever(database.productDao().retrieveData()).thenReturn(products)
        whenever(database.spotlightDao().retrieveData()).thenReturn(spotlight)
        whenever(database.cashDao().retrieveData()).thenReturn(cash)

        //when
        val actualProducts = repository.getProducts()

        //then
        assertEquals(actualProducts, expectedProducts)
    }

    @Test
    fun `getProducts() return null if server response is a failure and CacheDatabase does not have data`() = runBlocking {
        // given
        val expectedProducts = null
        val exception = mock<HttpException>()
        whenever(service.getProducts()).thenThrow(exception)
        whenever(database.productDao().retrieveData()).thenReturn(emptyList())
        whenever(database.spotlightDao().retrieveData()).thenReturn(emptyList())
        whenever(database.cashDao().retrieveData()).thenReturn(null)

        //when
        val actualProducts = repository.getProducts()

        //then
        assertEquals(actualProducts, expectedProducts)
    }
}