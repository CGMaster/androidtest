package br.com.digio.androidtest

import okhttp3.mockwebserver.MockWebServer
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


fun MockWebServer.getRetrofit(): Retrofit {
    return Retrofit.Builder()
        .baseUrl(this.url(""))
        .addConverterFactory(GsonConverterFactory.create())
        .build()
}