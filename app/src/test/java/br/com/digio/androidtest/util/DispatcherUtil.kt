package br.com.digio.androidtest

import br.com.digio.androidtest.util.ResourceLoader
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.RecordedRequest

val successDispatcher = object: Dispatcher(){
    override fun dispatch(request: RecordedRequest): MockResponse {
        val code = 200
        val fileName = request.path
            ?.replace("/","-")
            .orEmpty()
            .drop(1)
        return MockResponse()
            .setResponseCode(code)
            .setBody(ResourceLoader.loadJson(fileName, code))
    }
}