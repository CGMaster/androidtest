package br.com.digio.androidtest.util

import java.io.IOException
import java.io.InputStreamReader
import java.lang.StringBuilder


object ResourceLoader {
    fun loadJson(fileName: String, code: Int):String {
        try{
            val inputStream = javaClass.classLoader?.getResourceAsStream("api-response/${fileName}-${code}.json")
            val result = StringBuilder()
            InputStreamReader(inputStream, "UTF-8")
                .readLines()
                .forEach {
                    result.append(it)
                }
            return result.toString()
        } catch (e: IOException){
            throw e
        }
    }
}