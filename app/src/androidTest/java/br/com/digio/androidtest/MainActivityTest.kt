package br.com.digio.androidtest

import androidx.lifecycle.Lifecycle
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.rules.activityScenarioRule
import androidx.test.platform.app.InstrumentationRegistry
import br.com.digio.androidtest.ui.main.MainActivity
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class MainActivityTest {

    @get:Rule
    var activityScenarioRule = activityScenarioRule<MainActivity>()

    
    private val server = MockWebServer()
    private val context = InstrumentationRegistry.getInstrumentation().targetContext


    @Before
    fun setup(){
        server.dispatcher = MockServerUtil.dispatcherSuccess
        server.start(MockServerUtil.PORT)
    }

    @Test
    fun onDisplayTitleShouldHaveHello() {
            val title = context.getString(R.string.hello_maria)
            onView(withText(title)).check(matches(isDisplayed()))
    }

    @Test
    fun onDisplayListSpotlightShouldRechargeItem() {
            RecyclerViewMatchers.checkRecyclerViewItem(R.id.recyMainSpotlight, 0, isDisplayed())
    }


    @Test
    fun onDisplayListProductsShouldRechargeItem() {
            RecyclerViewMatchers.checkRecyclerViewItem(R.id.recyMainProducts, 0, isDisplayed())
    }


    @After
    fun tearDown(){
        server.close()
    }
}